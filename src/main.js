import { createApp } from "vue";
import { createPinia } from "pinia";
import "vuetify/styles";
import axios from "axios";
import { createVuetify } from "vuetify";
import * as components from "vuetify/components";
import * as directives from "vuetify/directives";
import "@mdi/font/css/materialdesignicons.css";
import { aliases, fa } from 'vuetify/iconsets/fa'
import { mdi } from "vuetify/iconsets/mdi";
import App from "./App.vue";
import router from "./router";
import vuesax from "vuesax3";
import "vuesax3/dist/vuesax.css";
import "../src/assets/styles/style.css";
import "../src/assets/styles/header.css";
import "../src/assets/styles/search.css";
import "../src/assets/styles/footer.css";

const axiosInstance = axios.create({
  withCredentials: true,
});
const pinia = createPinia();
const vuetify = createVuetify({
  components,
  directives,
  icons: {
    defaultSet: 'fa',
    aliases,
    sets: {
      fa,
      mdi,
    },
  },
});

const app = createApp(App);
app.use(router);
app.use(vuesax);
app.use(vuetify).use(pinia).mount("#app");
app.config.globalProperties.$axios = { ...axiosInstance };
