import { defineStore } from "pinia";

export const usePaginationStore = defineStore("pagination", {
  state() {
    return {
      lastPage: null,
    };
  },
});
