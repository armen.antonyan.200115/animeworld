import { defineStore } from "pinia";
import { usePaginationStore } from "./PaginationStore";
import axios from "axios";

const url = "https://api.jikan.moe/v4/anime";

export const useAnimeMoviesStore = defineStore("animeMovies", {
  state() {
    return {
      animeMovies: [],
      animeGenres: [],
    };
  },
  actions: {
    async fetchAnimeData() {
      const pagination = usePaginationStore();
      return await axios.get(url).then((response) => {
        this.animeMovies = response.data.data;
        pagination.lastPage = response.data.pagination.last_visible_page;
      });
    },
    async fetchAnimeBySearch(input) {
      return await axios
        .get(url + `?q=${input}`)
        .then((response) => (this.animeMovies = response.data.data));
    },
    async fetchAnimeBySearchGenres(genreID) {
      return await axios
        .get(url + `?q=genre=${genreID}`)
        .then((response) => (this.animeMovies = response.data.data));
    },
    async fetchAnimeGenres() {
      return await axios
        .get("https://api.jikan.moe/v4/genres/anime")
        .then((response) => (this.animeGenres = response.data.data));
    },
    async fetchAnimeYear(year){
      return await axios
        .get(url + `?q=start_date=${year}`)
        .then((response) => (this.animeMovies = response.data.data))
    },
    async fetchAnimeByPage(page) {
      return await axios
        .get(`https://api.jikan.moe/v4/anime?page=${page}`)
        .then((response) => (this.animeMovies = response.data.data));
    },
  },
});
