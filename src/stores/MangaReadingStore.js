import { defineStore } from "pinia";
import { usePaginationStore } from './PaginationStore'
import axios from "axios";

const url = "https://api.jikan.moe/v4/manga";

export const useMangaReadingStore = defineStore("mangaReading", {
  state() {
    return {
      mangaReading: [],
      mangaGenres: [],
    };
  },
  actions: {
    async fetchData() {
      const pagination = usePaginationStore()
      return await axios.get(url).then((response) => {
        this.mangaReading = response.data.data;
        pagination.lastPage = response.data.pagination.last_visible_page;
      });
    },
    async fetchMangaBySearch(input) {
      return await axios
        .get(url + `?q=${input}`)
        .then((response) => (this.mangaReading = response.data.data));
    },
    async fetchMangaBySearchGenres(genreID) {
      return await axios
        .get(url + `?q=genre=${genreID}`)
        .then((response) => (this.mangaReading = response.data.data));
    },
    async fetchMangaGenres() {
      return await axios
        .get("https://api.jikan.moe/v4/genres/manga")
        .then((response) => (this.mangaGenres = response.data.data));
    },
    async fetchMangaYear(year){
      return await axios
        .get(url + `?q=start_date=${year}`)
        .then((response) => (this.mangaReading = response.data.data))
    },
    async fetchMangaByPage(page) {
      return await axios
        .get(`https://api.jikan.moe/v4/manga?page=${page}`)
        .then((response) => (this.mangaReading = response.data.data));
    },
  },
});
