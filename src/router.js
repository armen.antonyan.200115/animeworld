import { createRouter, createWebHistory } from "vue-router";
import Manga from "./components/Manga.vue";
import AnimeLife from "./components/AnimeLife.vue";

export default createRouter({
  history: createWebHistory(),
  routes: [
    { path: "/", component: AnimeLife },
    { path: "/manga/", component: Manga },
    { path: "/anime/", component: AnimeLife },
  ],
});
